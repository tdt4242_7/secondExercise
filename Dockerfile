FROM heroku/heroku:16
ENV PYTHONUNBUFFERED 1
ADD . /code
WORKDIR /code
RUN apt-get update -y 
RUN apt-get -y install python3-pip
RUN pip3 install -r requirements.txt
RUN python3 manage.py migrate
from django.contrib import admin

from .models import *


class ItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'brand', 'stock', 'price')

# Register your models here.
admin.site.register(Brand)
admin.site.register(Tag)
admin.site.register(Item, ItemAdmin)
admin.site.register(DiscountPercentage)
admin.site.register(DiscountPackage)

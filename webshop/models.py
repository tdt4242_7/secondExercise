import math

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from secondExercise.helper_functions import OutOfStockError


# Create your models here.
class Brand(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta():
        verbose_name_plural = "Brands"
        db_table = "brands"


class Tag(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

    class Meta():
        verbose_name_plural = "Tags"
        db_table = "tags"


class Item(models.Model):
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    brand = models.ForeignKey("Brand", on_delete=models.CASCADE, default=1)
    tag = models.ManyToManyField("Tag")
    stock = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name

    def get_item_percentage_price(self):
        try:
            discount = DiscountPercentage.objects.get(item=self)
            return round(self.price * (1 - discount.percentage), 2)
        except ObjectDoesNotExist:
            return False

    # Returns the type of discount and how much it is
    def get_discount(self):
        try:
            # Percentage discount
            discount = DiscountPercentage.objects.get(item=self)
            return  ("{:2.0f}% off").format(discount.percentage * 100)
        except ObjectDoesNotExist:
            # Package discount
            try:
                discount = DiscountPackage.objects.get(item=self)
                return ("{} for {}").format(discount.buyx, discount.fory)
            except ObjectDoesNotExist:
                return False

    class Meta():
        verbose_name_plural = "Items"
        db_table = "items"


class DiscountPercentage(models.Model):
    item = models.ForeignKey("Item", on_delete=models.CASCADE, related_name='discount')
    percentage = models.DecimalField(max_digits=3, decimal_places=2)

    class Meta():
        db_table = "discount_percentages"


class DiscountPackage(models.Model):
    item = models.ForeignKey("Item", on_delete=models.CASCADE)
    buyx = models.IntegerField()
    fory = models.IntegerField()

    class Meta():
        db_table = "discount_packages"

    def get_new_amount(self, quantity):
        return (math.floor(quantity / self.buyx) * self.fory) + (quantity % self.buyx)


class Cart(models.Model):
    items = models.ManyToManyField('Order')
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return '%s' % self.owner

    def add_item(self, item):
        try:
            order = self.items.get(item=item)
            order.add_quantity(1)
        except ObjectDoesNotExist:
            self.items.add(Order.objects.create(item=item))
            self.save()

    def update_item(self, item, amount):
        try:
            order = self.items.get(item=item)
            if amount >= 1:
                order.update_quantity(amount)
            elif amount <= 0:
                self.delete_item(item)
            self.save()
        except ObjectDoesNotExist:
            self.items.add(Order.objects.create(item=item))
            self.save()

    def remove_item(self, item):
        try:
            order = self.items.get(item=item)
            order.quantity -= 1
            if order.quantity <= 0:
                self.delete_item(item)
                return
            order.save()
        except ObjectDoesNotExist:
            pass

    def delete_item(self, item):
        try:
            self.items.remove(self.items.get(item=item))
            self.save()
        except ObjectDoesNotExist:
            pass

    def update_stock(self):
        for order in self.items.all():
            order.item.stock -= order.quantity
            order.item.save()

    def get_total(self):
        total = 0
        for order in self.items.all()[:]:
            total += order.get_order_total()
        return total


class Order(models.Model):
    item = models.ForeignKey('Item', on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    # Increases quantity by amount if in stock.
    def add_quantity(self, amount):
        if self.item.stock < (self.quantity + amount):
            raise OutOfStockError()
        self.quantity += amount
        self.save()

    # Sets quantity by amount if in stock, or deletes order.
    def update_quantity(self, quantity):
        if self.item.stock < quantity:
            raise OutOfStockError()
        self.quantity = quantity
        self.save()


    # Calculates the order total
    def get_order_total(self):
        try:
            # Percentage discount
            # DiscountPercentage gives the percentile reduction
            # So we subtract it from 1
            discount = DiscountPercentage.objects.get(item=self.item)
            return round(self.item.price * (1 - discount.percentage) * self.quantity, 2)
        except ObjectDoesNotExist:
            try:
                #Package discount
                discount = DiscountPackage.objects.get(item=self.item)
                discount_total = self.item.price * int(self.quantity / discount.buyx) * discount.fory
                rest_total = self.item.price * (self.quantity % discount.buyx)
                return discount_total + rest_total
            except ObjectDoesNotExist:
                # No discount
                return round(self.item.price * self.quantity, 2)

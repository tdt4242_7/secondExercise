from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^search/$', views.search, name="search"),
    url(r'^accounts/login/$', views.custom_login, name='login'),
    url(r'^login/$', views.custom_login, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'logged_out.html'}, name='logout'),
    path('<int:item_id>/', views.detail, name='detail'),
    path('cart/', views.cart, name='cart'),
    path('cart/buy/', views.buy_cart, name='buy_cart_base'),
    path('cart/buy/<str:email>/', views.buy_cart, name='buy_cart'),
    path('add/<int:item_pk>/', views.add_item_to_cart, name='add_item_to_cart'),
    path('update/', views.update_item_in_cart, name='update_item_in_cart_base'),
    path('update/<int:item_pk>/<int:amount>/', views.update_item_in_cart, name='update_item_in_cart'),
    path('remove/<int:item_pk>/', views.remove_item_from_cart, name='remove_in_cart'),
    path('delete/<int:item_pk>/', views.delete_item_from_cart, name='delete_item_from_cart')
]

import django_filters
from django import forms
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.views import login
from django.shortcuts import redirect, get_object_or_404
from django.shortcuts import render

from secondExercise.helper_functions import OutOfStockError, send_confirmation_email
from .models import Item, Brand, Tag, Cart


@login_required
def index(request):
	item_list = ItemsFilter(request.GET, queryset=Item.objects.filter(stock__gt=0)[:5])
	context = {
		'item_list': item_list,
	}
	return render(request, "index.html", context)


def detail(request, item_id):
	item = get_object_or_404(Item, pk=item_id)
	context = {
		'item': item,
	}
	return render(request, 'detail.html', context)


def custom_login(request):
	if request.user.is_authenticated:
		return redirect('index')
	else:
		return login(request)


def signup(request):
	if request.user.is_authenticated:
		return redirect('index')

	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)
			login(request, user)
			return redirect('index')
	else:
		form = UserCreationForm()
	return render(request, 'signup.html', {'form': form})


def search(request):
	itemsFilter = ItemsFilter(request.GET, queryset=Item.objects.all())
	context = {
		'itemsFilter': itemsFilter,
	}
	return render(request, 'webshop/search.html', context)


@login_required()
def cart(request):
	cart, created = Cart.objects.get_or_create(owner=request.user, active=True)
	item_amount = 0
	total_price = cart.get_total
	for order in cart.items.all():
		item_amount += order.quantity
	context = {
		'cart': cart,
		'item_amount': item_amount,
		'total_price': total_price,
	}
	return render(request, 'webshop/cart.html', context)


@login_required()
def add_item_to_cart(request, item_pk):
	item = get_object_or_404(Item, pk=item_pk)
	cart, created = Cart.objects.get_or_create(owner=request.user, active=True)
	try:
		cart.add_item(item)
	except OutOfStockError:
		messages.add_message(request, messages.ERROR, 'The stock of this item can\'t support your desired order')
	return redirect('cart')\

@login_required()
def update_item_in_cart(request, item_pk, amount):
	item = get_object_or_404(Item, pk=item_pk)
	cart, created = Cart.objects.get_or_create(owner=request.user, active=True)
	try:
		cart.update_item(item, amount)
	except OutOfStockError:
		messages.add_message(request, messages.ERROR, 'The stock of this item can\'t support your desired order')
	return redirect('cart')\

@login_required()
def remove_item_from_cart(request, item_pk):
	item = get_object_or_404(Item, pk=item_pk)
	cart = get_object_or_404(Cart, owner=request.user, active=True)
	cart.remove_item(item)
	return redirect('cart')


@login_required()
def delete_item_from_cart(request, item_pk):
	item = get_object_or_404(Item, pk=item_pk)
	cart = Cart.objects.get(owner=request.user, active=True)
	cart.delete_item(item)
	return redirect('cart')


@login_required
def buy_cart(request, email):
	cart = get_object_or_404(Cart, owner=request.user, active=True)
	if(cart.get_total() <= 0):
		return redirect('cart')
	cart.update_stock()
	context = {
		'total': cart.get_total(),
		'cart': cart,
		'user': request.user
	}
	send_confirmation_email(email, context)
	cart.active = False
	cart.save()
	return render(request, 'webshop/reciept.html', context)


class ItemsFilter(django_filters.FilterSet):
	name = django_filters.CharFilter(lookup_expr="icontains")
	price = django_filters.RangeFilter()
	brand = django_filters.ModelChoiceFilter(queryset=Brand.objects.all())
	tag = django_filters.ModelMultipleChoiceFilter(widget=forms.CheckboxSelectMultiple, queryset=Tag.objects.all())

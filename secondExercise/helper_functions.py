from django.core.mail import send_mail
from django.template.loader import render_to_string


class OutOfStockError(Exception):
    pass


def send_confirmation_email(email, context):
    msg_plain = render_to_string('email/confirmation.txt', context)
    msg_html = render_to_string('email/confirmation.html', context)

    send_mail(
        "Receipt on Order id " + context['cart'].pk,
        msg_plain,
        'donotreply@group7webshop.com',
        [email],
        fail_silently=True,
        html_message=msg_html,
    )